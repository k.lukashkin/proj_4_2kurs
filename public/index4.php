<?php
header('Content-Type: text/html; charset=UTF-8');
if ($_SERVER['REQUEST_METHOD'] == 'GET') 
{ 
	$messages = array();
    $messages[8] = '';
  if (!empty($_COOKIE['save'])) 
  {
	setcookie('save', '', 100000);
    print('Results saved.');
  }
  $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['year'] = !empty($_COOKIE['birthday_error']);
    $errors['pol'] = !empty($_COOKIE['gender_error']);
    $errors['konech'] = !empty($_COOKIE['limbs_error']);
    $errors['bio'] = !empty($_COOKIE['biography_error']);
    $errors['ability'] = !empty($_COOKIE['ability_error']);
    $errors['contract'] = !empty($_COOKIE['contract_error']);
 if ($errors['fio']) {
      setcookie('fio_error', '', 100000);
      $messages[0] = '<div class="error_text">Empty name.</div>';
    }
    if ($errors['email']) {
        setcookie('email_error', '', 100000);
        $messages[1] = '<div class="error_text">ERROR!!! email@example.com.</div>';
    }
    if ($errors['year']) {
        setcookie('birthday_error', '', 100000);
        $messages[2] = '<div class="error_text">WTF.</div>';
    }
    if ($errors['pol']) {
      setcookie('gender_error', '', 100000);
      $messages[3] = '<div class="error_text">EMPTY GENDER.</div>';
  }
  if ($errors['konech']) {
    setcookie('limbs_error', '', 100000);
    $messages[4] = '<div class="error_text">No comments.</div>';
  }
    if ($errors['bio']) {
      setcookie('biography_error', '', 100000);
      $messages[6] = '<div class="error_text">Whats up?</div>';
    }
    if ($errors['ability']) {
      setcookie('ability_error', '', 100000);
      $messages[5] = '<div class="error_text">No ability.</div>';
    }
    if ($errors['contract']) {
      setcookie('contract_error', '', 100000);
      $messages[7] = '<div class="error_text">Press V.</div>';
    }
    $values = array();
    $values['fio'] = empty($_COOKIE['Name_value']) ? '' : $_COOKIE['Name_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['year'] = empty($_COOKIE['birthday_value']) ? '' : $_COOKIE['birthday_value'];
    $values['pol'] = empty($_COOKIE['gender_value']) ? '' : $_COOKIE['gender_value'];
    $values['konech'] = empty($_COOKIE['limbs_value']) ? '' : $_COOKIE['limbs_value'];
    $values['bio'] = empty($_COOKIE['biography_value']) ? '' : $_COOKIE['biography_value'];
    $ability = array();
    $ability = empty($_COOKIE['ability_values']) ? array() : unserialize($_COOKIE['ability_values'], ["allowed_classes" => false]);

  include('form.php');
}
else {
    $errors = FALSE;
    if (empty($_POST['fio'])) 
	{
      setcookie('fio_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else 
	{     
      setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }
    if (!preg_match('/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z0-9]+$/', $_POST['email'])) 
	{
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
    else
	{
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['year'])) 
	{
        setcookie('birthday_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
      }
      else 
	  {
        setcookie('birthday_value', $_POST['birthday'], time() + 30 * 24 * 60 * 60);
      }
    if (!isset($_POST['pol']))
	{
      setcookie('gender_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else 
	{
      setcookie('gender_value', $_POST['gender'], time() + 30 * 24 * 60 * 60);
    }
    if (!isset($_POST['konech'])) 
	{
      setcookie('limbs_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else 
	{
      setcookie('limbs_value', $_POST['limbs'], time() + 30 * 24 * 60 * 60);
    }
    if (empty($_POST['bio'])) 
	{
      setcookie('biography_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else 
	{
      setcookie('biography_value', $_POST['biography'], time() + 30 * 24 * 60 * 60);
    }

    if (!isset($_POST['ability'])) 
	{
      setcookie('ability_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }
    else {
      
      setcookie('ability_values', serialize($_POST['ability']), time() + 30 * 24 * 60 * 60);
    }
    if (!isset($_POST['contract'])) {
      setcookie('contract_error', '1', time() + 24 * 60 * 60);
      $errors = TRUE;
    }

if ($errors) 
{
    header('Location: index.php');
    exit();
  }
  else {
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('birthday_error', '', 100000);
    setcookie('gender_error', '', 100000);
    setcookie('limbs_error', '', 100000);
    setcookie('ability_error', '', 100000);
    setcookie('biography_error', '', 100000);
    setcookie('contract_error', '', 100000);
  }
$user = 'u35650';
$pass = '9782638';
$db = new PDO('mysql:host=localhost;dbname=u35650', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
$trimmedPost = [];
foreach ($_POST as $key => $value)
	if (is_string($value))
	$trimmedPost[$key] = trim($value);
	else
	$trimmedPost[$key] = $value;
try 
{
  $stmt = $db->prepare("INSERT INTO applic SET fio = ?, email = ?, year = ?, pol = ? ,konech = ?, bio = ?");
  $stmt -> execute([$_POST['Name'],$_POST['email'],$_POST['date'],$_POST['gender'],$_POST['numberOfLimbs'],$_POST['biography']]);

  $stmt2 = $db->prepare("INSERT INTO app SET id_user= ?, ab = ?");
  $id = $db->lastInsertId();
  foreach ($_Post['abilities'] as $s)
    $stmt2 -> execute([$id, $s]);
}
catch(PDOException $e){
  print('Error : ' . $e->getMessage());
  exit();
}
setcookie('save', '1');
header('Location: ?save=1');
}