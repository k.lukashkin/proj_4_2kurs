<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>2kurs</title>
    <link rel="stylesheet" href="Style.css">
  </head>
  <body>
      <main>
        <div class="div">
                <form action="index.php" method="POST">
                <label>
                  Name<br />
                  <input name="fio" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>"/>
                </label><br />
                <?php if ($errors['fio']) {print $messages[0];} ?>
        
                <label>
                  E-mail:<br />
                  <input name="email"
                  <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>"
                    type="email" />
                </label><br />
                <?php if ($errors['email']) {print $messages[1];} ?>
        
                <label>
                 Date of Birth:<br />
                  <input name="birthday"
                    <?php if ($errors['birthday']) {print 'class="error"';} ?> value="<?php print $values['birthday']; ?>"
                    type="date" min="1900-01-01" max="2022-12-31">
                </label><br />
                <?php if ($errors['birthday']) {print $messages[2];} ?>
                Gender:<br />
                <?php if ($errors['gender']) {print '<div class="error">';} ?> 
                <label><input type="radio"
                  name="gender" value="Male" <?php if($values['gender']=='Male') print "checked";else print ""; ?> />
                 Male
                </label>
                <label><input type="radio"
                  name="gender" value="Female" <?php echo ($values['gender']=='Female') ?  "checked" : "" ;  ?> />
                  Female
                </label>
                <label><input type="radio"
                  name="gender" value="Was" <?php echo ($values['gender']=='Was') ?  "checked" : "" ;  ?> />
                  Was
                </label>
                <br />
                <?php if ($errors['gender']) {print '</div>';print $messages[3];} ?>
                
        
                Number of limbs:<br />
                <?php if ($errors['limbs']) {print '<div class="error">';} ?> 
                <label><input type="radio"
                  name="limbs" value="0" <?php echo ($values['limbs']=='0') ?  "checked" : "" ;  ?>/>
                  0
                </label>
                <label><input type="radio"
                  name="limbs" value="2" <?php echo ($values['limbs']=='2') ?  "checked" : "" ;  ?>/>
                  2
                </label>
                <label><input type="radio"
                  name="limbs" value="3" <?php echo ($values['limbs']=='3') ?  "checked" : "" ;  ?>/>
                  3
                </label>
                <label><input type="radio"
                  name="limbs" value="10" <?php echo ($values['limbs']=='10') ?  "checked" : "" ;  ?>/>
                  10
                </label>
                <label><input type="radio"
                  name="limbs" value="100" <?php echo ($values['limbs']=='100') ?  "checked" : "" ;  ?>/>
                  100
                </label>
                <label><input type="radio"
                  name="limbs" value=")" <?php echo ($values['limbs']==')') ?  "checked" : "" ;  ?>/>
                  )
                </label>
                <br />
                <?php if ($errors['limbs']) {print '</div>';print $messages[4];} ?>       
                <?php echo (in_array("0",$ability)) ?  "selected" : ""  ; ?>
                <label>
                  Super abilities:
                  <br />
                  <select name="ability[]"
                    multiple="multiple" <?php if ($errors['ability']) {print 'class="error"';} ?>>
                    <option value="0" <?php echo (in_array("0",$ability)) ?  "selected" : ""  ; ?>>Levitation</option>
                    <option value="1" <?php echo (in_array('1',$ability)) ?  "selected" : ""  ; ?>>Walking through walls</option>
                    <option value="2" <?php echo (in_array('2',$ability)) ?  "selected" : ""  ; ?>>Undead</option>
                  </select>
                </label><br />
                <?php if ($errors['ability']) {print $messages[5];} ?>
        
                <label>
                   What's up guys?:<br />
                  <textarea name="biography" <?php if ($errors['biography']) {print 'class="error"';} ?>><?php echo $values['biography'] ;  ?></textarea>
                </label><br />
                <?php if ($errors['biography']) {print $messages[6];} ?>
                <br />
                <label><input type="checkbox"
                  name="contract" <?php if ($errors['contract']) {print 'class="error"';} ?>/>
                  Yes:
                </label><br />
                <?php if ($errors['contract']) {print $messages[7];} ?>
                
                <input id="submit" type="submit" value="YEEEEEEEEEES" />
              </form>
          </div>
      </main>
      <?php
if (!empty($messages)) {
print $messages[8];
}
?>
  <h1 class="fig">Задание 4</h1>
  <h3> CREATE TABLE applic(<h3>
<h3>CREATE TABLE application (</h3>
<h3>id int(10) unsigned NOT NULL AUTO_INCREMENT,</h3>
<h3>name varchar(128) NOT NULL DEFAULT '',</h3>
<h3>PRIMARY KEY (id));</h3>
<br>
<h3>create table applic(</h3>
<h3>id int (10) unsigned NOT NULL AUTO_INCREMENT,</h3>
<h3>fio varchar(28) NOT NULL DEFAULT '',</h3>
<h3>email varchar(28),</h3>
<h3>year varchar(28),</h3>
<h3>pol varchar (10), </h3>
<h3>konech varchar (10),</h3>
<h3>bio varchar(128),</h3>
<h3>PRIMARY KEY (id));</h3>
<br>
<h3>create table abilities(</h3>
<h3>id_user int(10) NOT NULL, </h3>
<h3>ab varchar (128));</h3>
</body>
</html>